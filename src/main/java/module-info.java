module org.refcodes.jobbus {
	requires org.refcodes.controlflow;
	requires org.refcodes.data;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.command;
	requires transitive org.refcodes.component;
	requires java.logging;

	exports org.refcodes.jobbus;
}
