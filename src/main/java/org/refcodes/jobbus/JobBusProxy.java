// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.jobbus;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.command.NoExceptionAvailableRuntimeException;
import org.refcodes.command.NoResultAvailableRuntimeException;
import org.refcodes.command.NotYetExecutedRuntimeException;
import org.refcodes.command.Undoable;
import org.refcodes.component.HandleTimeoutRuntimeException;
import org.refcodes.component.UnknownHandleRuntimeException;
import org.refcodes.controlflow.ControlFlowUtility;
import org.refcodes.controlflow.RetryTimeout;
import org.refcodes.data.RetryLoopCount;
import org.refcodes.data.SleepLoopTime;
import org.refcodes.exception.Trap;

/**
 * The {@link JobBusProxy} implements a {@link JobBus} proxy: a {@link JobBus}
 * proxy can use a remote {@link JobBus} whilst only delegating those operations
 * to the remote {@link JobBus} which can be handled on the remote system. Any
 * operation requiring asynchronous {@link Thread} operations is handled by the
 * {@link JobBusProxy}. This is necessary for the synchronous operations as a)
 * waiting for an asynchronous operation to finish may result in a timeout
 * caused by the remoting framework and as b) invoking lambda operations on the
 * proxy will not be propagated to a remote {@link JobBus} instance as
 * bidirectional remoting usually is not supported by a remoting framework.
 * 
 * @param <CTX> The context type to use, can by any component, service or POJO.
 * @param <H> The handle type used to reference a job.
 */
public class JobBusProxy<CTX, H> implements JobBus<CTX, H> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( JobBusProxy.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private JobBus<CTX, H> _jobBus;

	private ExecutorService _executorService;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new job bus proxy impl.
	 *
	 * @param aJobBus the job bus
	 */
	public JobBusProxy( JobBus<CTX, H> aJobBus ) {
		this( aJobBus, null );
	}

	/**
	 * Instantiates a new job bus proxy impl.
	 *
	 * @param aJobBus the job bus
	 * @param aExecutorService the executor service
	 */
	public JobBusProxy( JobBus<CTX, H> aJobBus, ExecutorService aExecutorService ) {
		_jobBus = aJobBus;
		if ( aExecutorService == null ) {
			_executorService = ControlFlowUtility.createCachedExecutorService( true );
		}
		else {
			_executorService = ControlFlowUtility.toManagedExecutorService( aExecutorService );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasHandle( H aHandle ) {
		return _jobBus.hasHandle( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasProgress( H aHandle ) {
		return _jobBus.hasProgress( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasReset( H aHandle ) {
		return _jobBus.hasReset( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasFlush( H aHandle ) {
		return _jobBus.hasFlush( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Undoable<CTX, ?, ?> lookupHandle( H aHandle ) {
		return _jobBus.lookupHandle( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Undoable<CTX, ?, ?> removeHandle( H aHandle ) {
		return _jobBus.removeHandle( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset( H aHandle ) {
		_jobBus.reset( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float getProgress( H aHandle ) {
		return _jobBus.getProgress( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush( H aHandle ) throws IOException {
		_jobBus.flush( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public H execute( Undoable<CTX, ?, ?> aJob ) {
		return _jobBus.execute( aJob );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isExecuted( H aHandle ) {
		return _jobBus.isExecuted( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasResult( H aHandle ) {
		return _jobBus.hasResult( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasException( H aHandle ) {
		return _jobBus.hasException( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <RET> RET getResult( H aHandle ) {
		return _jobBus.getResult( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Exception> E getException( H aHandle ) {
		return _jobBus.getException( aHandle );
	}

	/* Non-atomic methods to be implemented on proxy's system: */
	@Override
	public <RET, E extends Exception> void execute( Undoable<CTX, RET, E> aJob, final Consumer<RET> aResultConsumer ) {
		final H theHandle = _jobBus.execute( aJob );
		final Runnable theLambdaRunnable = () -> {
			waitForExecution( theHandle );
			try {
				aResultConsumer.accept( getResult( theHandle ) );
			}
			catch ( UnknownHandleRuntimeException | NotYetExecutedRuntimeException | NoResultAvailableRuntimeException e ) {
				LOGGER.log( Level.WARNING, "Unable to execute undoable job <" + aJob + "> as of: " + Trap.asMessage( e ), e );
			}
		};
		_executorService.execute( theLambdaRunnable );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <RET, E extends Exception> void execute( Undoable<CTX, RET, E> aJob, final BiConsumer<RET, E> aResultConsumer ) {
		final H theHandle = _jobBus.execute( aJob );
		final Runnable theLambdaRunnable = () -> {
			waitForExecution( theHandle );
			try {
				try {
					aResultConsumer.accept( getResult( theHandle ), null );
				}
				catch ( NoResultAvailableRuntimeException e ) {
					aResultConsumer.accept( null, getException( theHandle ) );
				}
			}
			catch ( UnknownHandleRuntimeException | NotYetExecutedRuntimeException | NoExceptionAvailableRuntimeException e ) {
				LOGGER.log( Level.WARNING, e.toMessage(), e );
			}
		};
		_executorService.execute( theLambdaRunnable );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void waitForExecution( H aHandle ) {
		while ( !_jobBus.isExecuted( aHandle ) ) {
			try {
				Thread.sleep( SleepLoopTime.NORM.getTimeMillis() );
			}
			catch ( InterruptedException e ) {
				break;
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void waitForExecution( H aHandle, long aTimeoutMillis ) {
		final RetryTimeout theRetryTimeout = new RetryTimeout( aTimeoutMillis, RetryLoopCount.NORM_NUM_RETRY_LOOPS.getValue() );
		while ( !_jobBus.isExecuted( aHandle ) && theRetryTimeout.hasNextRetry() ) {
			theRetryTimeout.nextRetry();
		}
		if ( !isExecuted( aHandle ) ) {
			throw new HandleTimeoutRuntimeException( "Execution of the command referenced by the given handle did not terminate in the given amount of <" + aTimeoutMillis + "> ms, aborting wait loop.", aHandle );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <JOB extends Undoable<CTX, RET, ?>, RET> RET getResult( JOB aJob ) {
		final H theHandle = execute( aJob );
		waitForExecution( theHandle );
		return _jobBus.getResult( theHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <JOB extends Undoable<CTX, RET, ?>, RET> RET getResult( JOB aJob, long aTimeoutMillis ) {
		final H theHandle = execute( aJob );
		waitForExecution( theHandle, aTimeoutMillis );
		return _jobBus.getResult( theHandle );
	}
}
