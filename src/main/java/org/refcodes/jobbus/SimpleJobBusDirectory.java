// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.jobbus;

import org.refcodes.command.Undoable;
import org.refcodes.component.HandleGenerator;
import org.refcodes.component.HandleGeneratorImpl;

/**
 * The {@link SimpleJobBusDirectory} is a ready to use implementation of a
 * composite {@link JobBusDirectory} extending the
 * {@link AbstractJobBusDirectory}.
 *
 * @param <CTX> The context type to use, can by any component, service or POJO.
 */
public class SimpleJobBusDirectory<CTX> extends AbstractJobBusDirectory<CTX, String> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates the {@link SimpleJobBusDirectory} with the provided context
	 * and and a pre-defined {@link String} objects generating
	 * {@link HandleGenerator}. It is up to you which context (service,
	 * {@link org.refcodes.component.Component}, POJO) you want to provide to a
	 * job ({@link Undoable}) when being executed.
	 * 
	 * @param aContext The context which is passed to the job ({@link Undoable})
	 *        instances when being executed.
	 */
	public SimpleJobBusDirectory( CTX aContext ) {
		super( aContext, new HandleGeneratorImpl() );
	}
}
