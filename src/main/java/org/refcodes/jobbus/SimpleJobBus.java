// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.jobbus;

import java.io.IOException;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.refcodes.command.Undoable;
import org.refcodes.component.HandleGenerator;

/**
 * The {@link SimpleJobBus} is a ready to use implementation of a composite
 * {@link JobBus} wrapping a {@link JobBusDirectory} (actually a {@link JobBus}
 * is sufficient) and delegating the method calls to the wrapped instances. The
 * {@link JobBusDirectory} is considered to be the master from which multiple
 * {@link JobBus} instances can be derived. In case none {@link JobBusDirectory}
 * is considered to be required, use the empty constructor which creates its
 * internal {@link JobBusDirectory}.
 * <p>
 * In case you want to extend the {@link SimpleJobBus} with setting concrete
 * parameters for the generic types, please also overwrite the hook method
 *
 * @param <CTX> The context type to use, can by any component, service or POJO.
 */
public class SimpleJobBus<CTX> implements JobBus<CTX, String> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final JobBus<CTX, String> _jobBus;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates the {@link SimpleJobBus} by using the provided
	 * {@link JobBus} to which the method calls are delegated. Usually this
	 * constructor is used when you created a master {@link JobBusDirectory}
	 * from which client {@link JobBus} instances are to be derived. In case you
	 * do not require a master {@link JobBusDirectory}, please use the
	 * constructor {@link #SimpleJobBus(Object)}, which constructs the
	 * {@link JobBus} delegate with the the provided context.
	 * 
	 * @param aJobBus The {@link JobBus} to delegate the method calls to.
	 */
	public SimpleJobBus( JobBus<CTX, String> aJobBus ) {
		_jobBus = aJobBus;
	}

	/**
	 * Instantiates the {@link SimpleJobBus} with the provided context and and a
	 * pre-defined {@link String} objects generating {@link HandleGenerator}. It
	 * is up to you which context (service,
	 * {@link org.refcodes.component.Component}, POJO) you want to provide to a
	 * job ({@link Undoable}) when being executed.
	 * 
	 * @param aContext The context which is passed to the job ({@link Undoable})
	 *        instances when being executed.
	 */
	public SimpleJobBus( CTX aContext ) {
		_jobBus = createJobBus( aContext );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasHandle( String aHandle ) {
		return _jobBus.hasHandle( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasProgress( String aHandle ) {
		return _jobBus.hasProgress( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasReset( String aHandle ) {
		return _jobBus.hasReset( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasFlush( String aHandle ) {
		return _jobBus.hasFlush( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float getProgress( String aHandle ) {
		return _jobBus.getProgress( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Undoable<CTX, ?, ?> lookupHandle( String aHandle ) {
		return _jobBus.lookupHandle( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset( String aHandle ) {
		_jobBus.reset( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush( String aHandle ) throws IOException {
		_jobBus.flush( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Undoable<CTX, ?, ?> removeHandle( String aHandle ) {
		return _jobBus.removeHandle( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute( Undoable<CTX, ?, ?> aJob ) {
		return _jobBus.execute( aJob );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <RET, E extends Exception> void execute( Undoable<CTX, RET, E> aJob, Consumer<RET> aResultConsumer ) {
		_jobBus.execute( aJob, aResultConsumer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <RET, E extends Exception> void execute( Undoable<CTX, RET, E> aJob, BiConsumer<RET, E> aResultConsumer ) {
		_jobBus.execute( aJob, aResultConsumer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void waitForExecution( String aHandle ) {
		_jobBus.waitForExecution( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void waitForExecution( String aHandle, long aTimeoutMillis ) {
		_jobBus.waitForExecution( aHandle, aTimeoutMillis );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <JOB extends Undoable<CTX, RET, ?>, RET> RET getResult( JOB aJob ) {
		return _jobBus.getResult( aJob );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <JOB extends Undoable<CTX, RET, ?>, RET> RET getResult( JOB aJob, long aTimeoutMillis ) {
		return _jobBus.getResult( aJob, aTimeoutMillis );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isExecuted( String aHandle ) {
		return _jobBus.isExecuted( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasResult( String aHandle ) {
		return _jobBus.hasResult( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasException( String aHandle ) {
		return _jobBus.hasException( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <RET> RET getResult( String aHandle ) {
		return _jobBus.getResult( aHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Exception> E getException( String aHandle ) {
		return _jobBus.getException( aHandle );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Hook method pre-implemented useful when extending this class.
	 * 
	 * @param aContext The context to be passed to the job ({@link Undoable})
	 *        instances when being executed.
	 * 
	 * @return The ready to use {@link JobBus}.
	 */
	protected JobBus<CTX, String> createJobBus( CTX aContext ) {
		return new SimpleJobBusDirectory<CTX>( aContext );
	}
}
