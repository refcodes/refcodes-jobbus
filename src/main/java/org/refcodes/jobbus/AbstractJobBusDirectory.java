// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.jobbus;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import org.refcodes.command.Undoable;
import org.refcodes.component.HandleGenerator;

/**
 * The {@link AbstractJobBusDirectory} extends the {@link AbstractJobBus} with
 * the {@link JobBusDirectory} functionality.
 *
 * @param <CTX> The context type to use, can by any component, service or POJO.
 * @param <H> The handle type used to reference a job.
 */
public abstract class AbstractJobBusDirectory<CTX, H> extends AbstractJobBus<CTX, H> implements JobBusDirectory<CTX, H> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates the {@link AbstractJobBusDirectory} with the provided
	 * context and the provided {@link HandleGenerator}. It is up to you which
	 * context (service, {@link org.refcodes.component.Component}, POJO) you
	 * want to provide to a job ({@link Undoable}) when being executed. Also you
	 * can provide any {@link HandleGenerator} you thing useful when creating
	 * handles. It is up to your {@link HandleGenerator} to generate unique
	 * handle objects. The {@link JobBusDirectory} actually uses a
	 * {@link String} objects generating {@link HandleGenerator}. Make sure your
	 * handles implement the {@link #hashCode()} and {@link #equals(Object)}
	 * methods as of their method contracts as them handles will be used in
	 * collections such as {@link HashMap} data structures.
	 * 
	 * @param aContext The context which is passed to the job ({@link Undoable})
	 *        instances when being executed.
	 * @param aHandleGenerator The {@link HandleGenerator} to be used when
	 *        generating unique handle objects.
	 */
	public AbstractJobBusDirectory( CTX aContext, HandleGenerator<H> aHandleGenerator ) {
		super( aContext, aHandleGenerator );
	}

	// /////////////////////////////////////////////////////////////////////////
	// DIRECTORY:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Handle references.
	 *
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Undoable<CTX, ?, ?>> handleReferences() {
		return super.handleReferences();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<H> handles() {
		return super.handles();
	}
}
