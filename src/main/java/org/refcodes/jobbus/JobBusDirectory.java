// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.jobbus;

import org.refcodes.command.Undoable;
import org.refcodes.component.Component;
import org.refcodes.component.FlushHandle;
import org.refcodes.component.HandleDirectory;
import org.refcodes.component.HandleLookup;
import org.refcodes.component.ProgressHandle;
import org.refcodes.component.ResetHandle;

/**
 * The {@link JobBusDirectory} is actually a plain extension of the
 * {@link JobBus} providing access to all the handles currently being managed by
 * the {@link JobBus}. The {@link JobBus} directory provides means to get a full
 * listing of currently known {@link Undoable} (job) instances and the according
 * handles. Only privileged systems should access the {@link JobBusDirectory} as
 * any system having access can intervene with the {@link Undoable} (job)
 * instances without owning those {@link Undoable} (job) instances (without
 * owning the according handle).
 *
 * @param <CTX> The context type to use, can by any {@link Component}, service
 *        or POJO.
 * @param <H> The handle type used to reference a job.
 */
public interface JobBusDirectory<CTX, H> extends JobBus<CTX, H>, HandleDirectory<H, Undoable<CTX, ?, ?>>, HandleLookup<H, Undoable<CTX, ?, ?>>, ProgressHandle<H>, ResetHandle<H>, FlushHandle<H> {}
