// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.jobbus;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.refcodes.command.AbstractUndoable;
import org.refcodes.command.Undoable;
import org.refcodes.component.HandleTimeoutRuntimeException;

/**
 * The Class JobBusProxyTest.
 */
public class JobBusProxyTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String APPEND_TEXT = "123456789101112131415161718192021222324252627282930313233343536373839404142434445464748495051525354555657585960616263646566676869707172737475767778798081828384858687888990919293949596979899100";
	private static final String DONE = "Done!";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final List<Integer> _context = new ArrayList<>();
	private JobBus<List<Integer>, String> _jobBus;
	private int _ret;

	// /////////////////////////////////////////////////////////////////////////
	// SETUP:
	// /////////////////////////////////////////////////////////////////////////

	@BeforeEach
	public void beforeEach() {
		_jobBus = new JobBusProxy<>( new SimpleJobBus<List<Integer>>( _context ) );
		for ( int i = 1; i <= 100; i++ ) {
			_context.add( i );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// SETUP:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testGetResult() throws Exception {
		final SumCommand theSumCommand = new SumCommand();
		final MulCommand theMulCommand = new MulCommand();
		final AppendCommand theAppendCommand = new AppendCommand();
		final int theSumResult = _jobBus.getResult( theSumCommand );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( Integer.toString( theSumResult ) );
		}
		final Double theMulResult = _jobBus.getResult( theMulCommand );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( Double.toString( theMulResult ) );
		}
		final String theAppendResult = _jobBus.getResult( theAppendCommand );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theAppendResult );
		}
		assertEquals( 5050, theSumResult );
		assertEquals( "9.33262154439441E157", theMulResult.toString() );
		assertEquals( APPEND_TEXT, theAppendResult );
	}

	@Test
	public void testExecute() throws Exception {
		final SumCommand theSumCommand = new SumCommand();
		final MulCommand theMulCommand = new MulCommand();
		final AppendCommand theAppendCommand = new AppendCommand();
		final String theSumHandle = _jobBus.execute( theSumCommand );
		final String theMulHandle = _jobBus.execute( theMulCommand );
		final String theAppendHandle = _jobBus.execute( theAppendCommand );
		_jobBus.waitForExecution( theSumHandle );
		final int theSumResult = _jobBus.getResult( theSumHandle );
		_jobBus.waitForExecution( theMulHandle );
		final Double theMulResult = _jobBus.getResult( theMulHandle );
		_jobBus.waitForExecution( theAppendHandle );
		final String theAppendResult = _jobBus.getResult( theAppendHandle );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( Integer.toString( theSumResult ) );
			System.out.println( Double.toString( theMulResult ) );
			System.out.println( theAppendResult );
		}
		assertEquals( 5050, theSumResult );
		assertEquals( "9.33262154439441E157", theMulResult.toString() );
		assertEquals( APPEND_TEXT, theAppendResult );
		final Undoable<?, ?, ?> theAddCommand2 = _jobBus.lookupHandle( theSumHandle );
		assertEquals( theSumCommand, theAddCommand2 );
		final Undoable<?, ?, ?> theMulCommand2 = _jobBus.lookupHandle( theMulHandle );
		assertEquals( theMulCommand, theMulCommand2 );
		final Undoable<?, ?, ?> theAppendCommand2 = _jobBus.lookupHandle( theAppendHandle );
		assertEquals( theAppendCommand, theAppendCommand2 );
	}

	@Test
	public void testExecuteConsumer() {
		_ret = -1;
		final SumCommand theSumCommand = new SumCommand();
		_jobBus.execute( theSumCommand, ( ret ) -> {
			assertTrue( ret == 5050 );
			_ret = ret;
			synchronized ( this ) {
				notifyAll();
			}
		} );
		synchronized ( this ) {
			try {
				wait( 3000 );
			}
			catch ( InterruptedException ignored ) {}
		}
		assertTrue( _ret == 5050 );
	}

	@Test
	public void testExecuteBiConsumer() {
		_ret = -1;
		final SumCommand theSumCommand = new SumCommand();
		_jobBus.execute( theSumCommand, ( ret, e ) -> {
			assertNull( e );
			assertTrue( ret == 5050 );
			_ret = ret;
			synchronized ( this ) {
				notifyAll();
			}
		} );
		synchronized ( this ) {
			try {
				wait( 3000 );
			}
			catch ( InterruptedException ignored ) {}
		}
		assertTrue( _ret == 5050 );
	}

	@Test
	public void testWaitForTimeout() {
		TimeoutCommand theTimeoutCommand = new TimeoutCommand( 5000 );
		String theTimeoutHandle = _jobBus.execute( theTimeoutCommand );
		try {
			_jobBus.waitForExecution( theTimeoutHandle, 100 );
			fail( "Expecting to termintate with an exception as the job takes longer for execution than the wait timeout takes for waiting." );
		}
		catch ( HandleTimeoutRuntimeException e ) {
			// Expected
		}
		theTimeoutCommand = new TimeoutCommand( 5000 );
		try {
			final String theResult = _jobBus.getResult( theTimeoutCommand, 100 );
			assertEquals( DONE, theResult );
			fail( "Expecting to termintate with an exception as the job takes longer for execution than the wait timeout takes for waiting." );
		}
		catch ( HandleTimeoutRuntimeException e ) {
			// Expected
		}
		theTimeoutCommand = new TimeoutCommand( 100 );
		theTimeoutHandle = _jobBus.execute( theTimeoutCommand );
		try {
			_jobBus.waitForExecution( theTimeoutHandle, 5000 );
		}
		catch ( HandleTimeoutRuntimeException e ) {
			fail( "Expecting to termintate in time as the job takes shorter for execution than the wait timeout takes for waiting." );
		}
		theTimeoutCommand = new TimeoutCommand( 100 );
		try {
			final String theResult = _jobBus.getResult( theTimeoutCommand, 5000 );
			assertEquals( DONE, theResult );
			// Expected
		}
		catch ( HandleTimeoutRuntimeException e ) {
			fail( "Expecting to termintate in time as the job takes shorter for execution than the wait timeout takes for waiting." );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private class SumCommand extends AbstractUndoable<List<Integer>, Integer, Exception> {

		@Override
		public Integer execute( List<Integer> aContext ) {
			int theResult = 0;
			for ( Integer eInt : aContext ) {
				theResult += eInt;
			}
			return theResult;
		}
	}

	private class MulCommand extends AbstractUndoable<List<Integer>, Double, Exception> {

		@Override
		public Double execute( List<Integer> aContext ) {
			if ( aContext != null && aContext.size() > 0 ) {
				double theResult = aContext.get( 0 );
				final Iterator<Integer> e = aContext.iterator();
				e.next(); // We already set the first value:
				Integer eInt;
				while ( e.hasNext() ) {
					eInt = e.next();
					theResult *= eInt;
				}
				return theResult;
			}
			else {
				return 0D;
			}
		}
	}

	private class AppendCommand extends AbstractUndoable<List<Integer>, String, Exception> {

		@Override
		public String execute( List<Integer> aContext ) {
			final StringBuilder theResult = new StringBuilder();
			for ( Integer eInt : aContext ) {
				theResult.append( eInt );
			}
			return theResult.toString();
		}
	}

	private class TimeoutCommand extends AbstractUndoable<List<Integer>, String, Exception> {
		private final long _timeout;

		public TimeoutCommand( long aTimeout ) {
			_timeout = aTimeout;
		}

		@Override
		public String execute( List<Integer> aContext ) {
			synchronized ( this ) {
				try {
					Thread.sleep( _timeout );
				}
				catch ( InterruptedException ignored ) {}
			}
			return DONE;
		}
	}
}
