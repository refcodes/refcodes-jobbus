# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This artifact makes use of the [Command pattern](http://en.wikipedia.org/wiki/Command_pattern) and provides a frame to work with [`Command`](https://www.javadoc.io/doc/org.refcodes/refcodes-command/latest/org.refcodes.command/org/refcodes/command/Command.html) (`job`) instances (also in a distributed environments, e.g. REST, SOA, Cloud Computing) and provides do/undo functionality.***

## Getting started ##

> Please refer to the [refcodes-jobbus: Asynchronous job execution](https://www.metacodes.pro/refcodes/refcodes-jobbus) documentation for an up-to-date and detailed description on the usage of this artifact.  

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<groupId>org.refcodes</groupId>
		<artifactId>refcodes-jobbus</artifactId>
		<version>3.3.9</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-jobbus). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-jobbus).

## Contribution guidelines ##


* [Report issues](https://bitbucket.org/refcodes/refcodes-jobbus/issues)
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.